import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {
	   

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================

      double m;
      double x = LiesDoubleWertEin("Geben Sie den ersten Wert ein:");
      double y = LiesDoubleWertEin("Geben Sie den zweiten Wert ein:");
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = MittelwertBerechnen(x, y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
public static double LiesDoubleWertEin (String frage) {
    double zahl;
    Scanner scan = new Scanner(System.in);
	System.out.println(frage);
	zahl = scan.nextDouble();
	return zahl;
} 
private static double MittelwertBerechnen(double x, double y) {
	double m;
	m = (x + y) / 2.0;
	return m;
}
}
